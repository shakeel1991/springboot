package com.springboot.exception;

public class ErrorResponse {

	private int errorCode;
	private String errormessage;
	private String description;

	public ErrorResponse() {
	}

	public ErrorResponse(int errorCode, String errormessage, String description) {
		super();
		this.errorCode = errorCode;
		this.errormessage = errormessage;
		this.description = description;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrormessage() {
		return errormessage;
	}

	public void setErrormessage(String errormessage) {
		this.errormessage = errormessage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ErrorResponse [errorCode=" + errorCode + ", errormessage=" + errormessage + ", description="
				+ description + "]";
	}

}
