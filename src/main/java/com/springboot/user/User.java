package com.springboot.user;

import java.util.Date;

public class User {

	private Integer userId;
	private String userName;
	private Date birthDate;

	public User() {

	}

	public User(Integer userId, String userName, Date birthDate) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.birthDate = birthDate;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		return "UserServiceDao [userId=" + userId + ", userName=" + userName + ", birthDate=" + birthDate + "]";
	}

}
