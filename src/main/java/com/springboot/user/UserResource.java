package com.springboot.user;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = "User")
public class UserResource {

	@Autowired
	private UserServiceDao serviceDao;

	@GetMapping(path = "/users")
	public List<User> getUsers() {
		return serviceDao.getUsers();
	}

	@GetMapping("/users/{userId}")
	public User getUserById(
			@ApiParam(name = "Id", type = "Integer", value = "Id of the user", required = true) @RequestParam int userId) {
		User returnedUser = serviceDao.getUserById(userId);
		if (returnedUser == null) {
			throw new UserNotFoundException("User not found for ID : " + userId);
		}
		return returnedUser;
	}

	@PostMapping(path = "/users")
	public ResponseEntity<User> createUser(@RequestBody User user) {
		User createdUser = serviceDao.createUser(user);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{userId}")
				.buildAndExpand(createdUser.getUserId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping("/users/{userId}")
	public void deleteUserById(@PathVariable int userId) {
		User returnedUser = serviceDao.deleteUserById(userId);
		if (returnedUser == null) {
			throw new UserNotFoundException("User not found for ID : " + userId);
		}
	}

}
