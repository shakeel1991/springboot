package com.springboot.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

@Component
public class UserServiceDao {

	private static int incrementUserId = 3;

	private static List<User> availableUsers = new ArrayList<>();

	static {
		availableUsers.add(new User(1, "Rock", new Date()));
		availableUsers.add(new User(2, "Stone Cold", new Date()));
		availableUsers.add(new User(3, "RVD", new Date()));
	}

	public List<User> getUsers() {
		return availableUsers;
	}

	public User getUserById(int userId) {
		Optional<User> optional = availableUsers.stream().filter(user -> user.getUserId() == userId).findAny();
		if (optional.isPresent()) {
			return optional.get();
		} else {
			return null;
		}
	}

	public User createUser(User user) {

		if (user.getUserId() == null) {
			user.setUserId(++incrementUserId);
		}
		availableUsers.add(user);
		return user;
	}

	public User deleteUserById(int userId) {
		Optional<User> optional = availableUsers.stream().filter(user -> user.getUserId() == userId).findAny();
		if (optional.isPresent()) {
			availableUsers.remove(optional.get());
			return optional.get();
		} else {
			return null;
		}
	}

}
